import React, { Component, Suspense } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import CircularProgress from "@material-ui/core/CircularProgress";

// LazyLoading React Components
const LogIn = React.lazy(() => import("./pages/auth/login"));
class App extends React.Component {
 
  render(){

    return (
      <div className="App">
        <p>Hello world!</p>
      </div>
    );
  }

}

export default App;
