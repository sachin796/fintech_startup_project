require("dotenv").config();
const express = require('express')
const app = express()

var db = require("./models");

const PORT = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

var syncOptions = {};
syncOptions.force = process.env.SYNC_MODEL === "true" ? true : false;

// Starting the server, syncing our models ------------------------------------/
db.sequelizeConnection.sync(syncOptions).then(function() {
  app.listen(PORT, function() {
    console.log(
      "==> 🌎  Listening on port %s. Visit http://localhost:%s/ in your browser.",
      PORT,
      PORT
    );
  });
});
