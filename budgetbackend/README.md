Instructions to start

1. Create a .env file and put the contents provided and keep the variable inside .env as true
2. Change the config-->config.json-->development object according to your local MYSQLWorkBench username and password.
3. Create a schema/database inside the MYSQLWorkBench called budget_tracker.
4. After all the above steps are done sucessfully only then run npm start.
5. After that change the variable provided inside .env to false