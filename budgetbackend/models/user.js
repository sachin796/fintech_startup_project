
module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define("User", {
//   similar to creating firstname column in database with datatype string and allownull checkbox checked
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email:{
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      contact:{
        type: DataTypes.STRING(50)
      },
    password: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        len: [1]
      }
    }
  },
  {
    indexes: [
      // Create a unique index on username
      {
        unique: true,
        fields: ["email"]
      }
    ]
  }
  );

  return User;
};
