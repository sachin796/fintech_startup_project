
module.exports = function(sequelize, DataTypes) {
  var Categories = sequelize.define("Categories", {
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email:{
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      contact:{
        type: DataTypes.STRING(50)
      },
    password: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        len: [1]
      }
    }
  },
  {
    indexes: [
      // Create a unique index on email
      {
        unique: true,
        fields: ["email"]
      }
    ]
  }
  );

  return Categories;
};
